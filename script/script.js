






const todoList=( ) => {

    const todoListWrapper=document.createElement('div');
    document.body.appendChild(todoListWrapper);
    todoListWrapper.classList.add('todo__list__wrapper')
    
    const header=document.createElement('h1');
    header.innerHTML='TO DO LIST'
    todoListWrapper.appendChild(header);
    
    const formW=document.createElement('form');
    formW.classList.add('form')
    todoListWrapper.appendChild(formW);
    
    const inputEnter=document.createElement("input");
    inputEnter.classList.add('input__enter')
    inputEnter .setAttribute('placeholder', 'Enter your task here');
    formW.appendChild(inputEnter);
    
    const submit=document.createElement("input");
    submit.classList.add('btn__add')
    submit.setAttribute('value', 'ADD TASK');
    submit.setAttribute('type', 'submit');
    formW.appendChild(submit);
    
    const list=document.createElement('ol');
    list.classList.add('todolist')
    todoListWrapper.appendChild(list);
    
    const btnClear=document.createElement('button');
    btnClear.classList.add('clear')
    btnClear.innerHTML='Clear'
    todoListWrapper.appendChild(btnClear);
    



    const form = document.querySelector('.form');

    const inputTask = document.querySelector('.input__enter');

    const addButton = document.querySelector('.btn__add');

    const toDoList = document.querySelector('.todolist');

    const clear = document.querySelector('.clear');


    form.addEventListener('submit', function (event) {



        event.preventDefault();
        if (inputTask.value =='') {
        alert('Enter a task');
    } else{
        const newTask = createNewItem(inputTask.value);
        toDoList.appendChild(newTask);
        inputTask.value = '';
        inputTask.focus();
        
    }
        clear.addEventListener('click', function () {
        toDoList.innerHTML = ' ';
    });
    });

    function createNewItem(inputValue) {
      const task = document.createElement('li');
      task.classList.add('list')
      const span = document.createElement('span');
      const delChecked = document.createElement('input');
      const editBtn = document.createElement('button');
      editBtn.classList.add('edit__btn')
      editBtn.innerHTML = 'Edit';
      delChecked .setAttribute('type', 'checkbox');
      delChecked.classList.add('checked')

      span.innerHTML= inputValue;


      task.appendChild(span);
      task.appendChild(delChecked);
      task.appendChild(editBtn);

      delChecked .addEventListener('click', function () {
          task.classList.toggle('class')
          console.log(delChecked);
      });

      editBtn.addEventListener('click', function () {
          span.contentEditable = true;
          span.focus();
      });

      return task;

    }

    let style =document.createElement('style');
      document.head.appendChild(style);

    style.innerHTML=

        `
        body{
          background-color: crimson;
      }
      .todo__list__wrapper{
        margin: 0 auto;
        width: 95%;
        border-radius: 11px;
        text-align: center;
        background-color: #fff;
        padding: 10px;
    }
        .todolist{
          margin: 27px 64px;
        }
        .input__enter{
          width: 301px;
          height: 26px;
        }
        .btn__add{
        width: 100px;
        height: 33px;
        margin-left: 50px;
        color: crimson;
        background-color: #fff;
        border: 2px solid crimson;
        border-radius: 4px;
        font-size: 14px;
        }
        .clear:hover, .btn__add:hover,.edit__btn:hover{
          cursor:pointer;
          box-shadow: 5px 2px 15px 4px #efd6f7;
          transform: scale(1.1);
          transition-duration:0.5s;
      }
        
        .list{
          padding: 20px;
          display: grid;
          grid-template-columns: 1fr 50px 62px;
          justify-items: start;
          font-size: 30px;
        }
        .class{
          text-decoration: line-through;
          text-decoration-color:crimson;
          color: #d0c5c5;
        

      }
      .checked{
        width: 20px;
        height: 30px;
        border-radius: 10px;
        background-color: crimson;
      }
      .clear{
        margin-top: 30px;
        width: 146px;
        height: 47px;
        margin-left: 50px;
        color: crimson;
        background-color:crimson;
        border: 2px solid crimson;
        border-radius: 4px;
        font-size: 14px;
        color:#ffff;
      }
      .edit__btn{
        width: 51px;
        height: 37px;
        margin-left: 20px;
        /* color: crimson; */
        background-color: #d0c5c5;
        border: 2px solid #8e8a8a;
        border-radius: 28px;
        font-size: 15px;

      }`;

}




todoList()





